﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_FormularioDeContacto.Models
{
    public class DateRange
    {
        public string FechaInicial { get; set; }
        public string FechaFinal { get; set;  }
    }
}
