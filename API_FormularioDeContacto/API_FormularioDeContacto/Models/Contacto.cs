﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_FormularioDeContacto.Models
{
    public class Contacto
    {
        public int ContactoId { get; set; }
        public string nombre { get; set; }
        public string correo { get; set; }
        public string telefono { get; set; }
        public DateTime fecha { get; set; }

    }
}
