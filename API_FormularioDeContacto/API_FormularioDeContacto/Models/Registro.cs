﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_FormularioDeContacto.Models
{
    public class Registro
    {
        public int registroId { get; set; }
        public string nombre { get; set; }
        public string apellidoPaterno { get; set; }
        public string apellidoMaterno { get; set; }
        public string carrera { get; set; }
        public string nombreInstitucion { get; set; }
        public string correo { get; set; }
        public string nivelEducativo { get; set; }
        public string grado { get; set; }
    }
}
