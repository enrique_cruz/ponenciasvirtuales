﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_FormularioDeContacto.Models
{
    public class Usuario
    {
        public int usuarioId { get; set; }
        public string nombreUsuario { get; set; }
        public string correoUsuario { get; set; }
        public string passwordUsuario { get; set; }
    }
}
