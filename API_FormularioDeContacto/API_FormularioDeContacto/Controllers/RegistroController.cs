﻿using API_FormularioDeContacto.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace API_FormularioDeContacto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegistroController : Controller
    {
        private readonly IConfiguration _configuration;

        public RegistroController(IConfiguration configuration)
        {

            _configuration = configuration;
        }

        [EnableCors("AllowOrigin")]
        [HttpGet("All")]
        public JsonResult GetAll()
        {
            string SP_NAME = "GetLast50";
            string CON_STR = _configuration.GetConnectionString("PonenciasDB");
            DataTable dt = new DataTable();
            SqlDataReader rd;

            using (SqlConnection con = new SqlConnection(CON_STR))
            {

                using (SqlCommand cmd = new SqlCommand(SP_NAME, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;

                    con.Open();
                    rd = cmd.ExecuteReader();
                    dt.Load(rd);

                    rd.Close();
                    con.Close();

                }

            }

            return new JsonResult(dt);

        }

        [EnableCors("AllowOrigin")]
        [HttpPost("Date")]
        public JsonResult GetByDate(DateRange dr)
        {

            string SP_NAME = "GetByFecha";
            string CON_STR = _configuration.GetConnectionString("PonenciasDB");
            DataTable dt = new DataTable();
            SqlDataReader rd;

            using (SqlConnection con = new SqlConnection(CON_STR))
            {

                using (SqlCommand cmd = new SqlCommand(SP_NAME, con))
                {
                    var fi = dr.FechaInicial.Replace('-', '/');
                    var ff = dr.FechaInicial.Replace('-', '/');

                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.AddWithValue("@FECHA_INICIAL", dr.FechaInicial);
                    //cmd.Parameters.AddWithValue("@FECHA_FINAl", dr.FechaFinal);
                    cmd.Parameters.AddWithValue("@FECHA_INICIAL", fi);
                    cmd.Parameters.AddWithValue("@FECHA_FINAl", ff);

                    con.Open();
                    rd = cmd.ExecuteReader();
                    dt.Load(rd);

                    rd.Close();
                    con.Close();

                }

            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowOrigin")]
        [HttpPost("Nuevo")]
        public JsonResult Post(Registro registro)
        {
            string SP_NAME = "Registrar";
            string CON_STR = _configuration.GetConnectionString("PonenciasDB");
            string result = "Err";

            using (SqlConnection con = new SqlConnection(CON_STR))
            {

                using (SqlCommand cmd = new SqlCommand(SP_NAME, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@nombre", registro.nombre);
                    cmd.Parameters.AddWithValue("@apellidoPaterno", registro.apellidoPaterno);
                    cmd.Parameters.AddWithValue("@apellidoMaterno", registro.apellidoMaterno);
                    cmd.Parameters.AddWithValue("@carrera", registro.carrera);
                    cmd.Parameters.AddWithValue("@nombreInstitucion", registro.nombreInstitucion);
                    cmd.Parameters.AddWithValue("@correo", registro.correo);
                    cmd.Parameters.AddWithValue("@nivelEducativo", registro.nivelEducativo);
                    cmd.Parameters.AddWithValue("@grado", registro.grado);

                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    result = "OK";

                }

            }

            return new JsonResult(result);
        }
    }
}
