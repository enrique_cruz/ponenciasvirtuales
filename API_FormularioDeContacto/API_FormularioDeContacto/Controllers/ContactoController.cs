﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using API_FormularioDeContacto.Models;
using Microsoft.AspNetCore.Cors;

namespace API_FormularioDeContacto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactoController : Controller
    {
        private readonly IConfiguration _configuration;

        public ContactoController(IConfiguration configuration) {

            _configuration = configuration;
        }

        [EnableCors("AllowOrigin")]
        [HttpGet]
        public JsonResult Get()
        {
            string SP_NAME = "sp_Contacto_GetLast50";
            string CON_STR = _configuration.GetConnectionString("ContactoDB");
            DataTable dt = new DataTable();
            SqlDataReader rd;
          
            using (SqlConnection con = new SqlConnection(CON_STR)) {

                using (SqlCommand cmd= new SqlCommand (SP_NAME, con)) {
                    
                    cmd.CommandType = CommandType.StoredProcedure;

                    con.Open();
                    rd = cmd.ExecuteReader();
                    dt.Load(rd);

                    rd.Close();
                    con.Close();

                }

            }
                      
            return new JsonResult(dt);
            
        }

        [EnableCors("AllowOrigin")]
        [HttpPost("GetByDate")]
        public JsonResult GetByDate(DateRange dr) 
        {            

            string SP_NAME = "sp_ContactoGetByFecha";
            string CON_STR = _configuration.GetConnectionString("ContactoDB");
            DataTable dt = new DataTable();
            SqlDataReader rd;

            using (SqlConnection con = new SqlConnection(CON_STR))
            {

                using (SqlCommand cmd = new SqlCommand(SP_NAME, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FECHA_INICIAL", dr.FechaInicial);
                    cmd.Parameters.AddWithValue("@FECHA_FINAl", dr.FechaFinal);

                    con.Open();
                    rd = cmd.ExecuteReader();
                    dt.Load(rd);

                    rd.Close();
                    con.Close();

                }

            }

            return new JsonResult(dt);
        }

        [EnableCors("AllowOrigin")]
        [HttpPost]
        public JsonResult Post(Contacto contacto) 
        {
            string SP_NAME = "sp_Contacto_InsertarContacto";
            string CON_STR = _configuration.GetConnectionString("ContactoDB");
            string result = "Err";

            using (SqlConnection con = new SqlConnection(CON_STR))
            {

                using (SqlCommand cmd = new SqlCommand(SP_NAME, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@NOMBRE_COMPLETO", contacto.nombre);
                    cmd.Parameters.AddWithValue("@CORREO", contacto.correo);
                    cmd.Parameters.AddWithValue("@TELEFONO", contacto.telefono);

                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    result = "OK";

                }

            }

            return new JsonResult(result);
        }       
    }
}
