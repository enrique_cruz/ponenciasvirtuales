﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using API_FormularioDeContacto.Models;
using Microsoft.AspNetCore.Cors;

namespace API_FormularioDeContacto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : Controller
    {
        private readonly IConfiguration _configuration;

        public UsuarioController(IConfiguration configuration)
        {

            _configuration = configuration;
        }

        [EnableCors("AllowOrigin")]
        [HttpPost]
        public JsonResult Post(Usuario usuario)
        {
            string SP_NAME = "sp_Usuarios_Login";
            string CON_STR = _configuration.GetConnectionString("ContactoDB");
            DataTable dt = new DataTable();
            SqlDataReader rd;

            using (SqlConnection con = new SqlConnection(CON_STR))
            {

                using (SqlCommand cmd = new SqlCommand(SP_NAME, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CORREO_USUARIO", usuario.correoUsuario);
                    cmd.Parameters.AddWithValue("@PASSWORD_USUARIO", usuario.passwordUsuario);

                    con.Open();
                    rd = cmd.ExecuteReader();
                    dt.Load(rd);

                    rd.Close();
                    con.Close();
                }

            }

            return new JsonResult(dt);            

        }

    }
}
