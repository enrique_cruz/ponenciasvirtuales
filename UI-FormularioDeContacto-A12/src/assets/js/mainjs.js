﻿
$(function () {
    $('#enviar').click(function () {
        var contacto = {}
        contacto.nombre = $('#nombre').val();
        contacto.correo = $('#correo').val();
        contacto.telefono = $('#telefono').val();
        contacto.fecha = "";

        var nombre = $('#nombre').val();
        var correo = $('#correo').val();
        var telefono = $('#telefono').val();

        console.log(JSON.stringify(contacto));

        //VALIDAMOS QUE NO HAYA DATOS VACIOS
        if (nombre && correo && telefono ) {
            
            //VALIDAMOS LONGITUD DEL TELEFONO - 10 DÍGITOS
            if (telefono.length < 10) {
                alert("El teléfono debe ser de 10 dígitos (LADA + Número telefónico)")
            }
            else {
                //VALIDAMOS EMAIL
                if (ValidateEmail(correo)) {
                    //AJAX PARA SUBIR LOS DATOS
                    $.ajax({
                        type: "POST",
                        dataType: "json",                                                
                        //url:'https://monitor.grupopissa.com.mx/contacFor/Views/Home/GuardarContacto',
                        url:'/Home/GuardarContacto',
                        //url:'@(Url.Action("GuardarContacto", "Home"))',
                        data:JSON.stringify(contacto),
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            alert("Datos guardados correctamente !");
                            console.log("Datos guardados correctamente: " + data);
                            LimpiarCampos();                           
                        },
                        error: function (xhr, status, error) {
                            //function (xhr, status, error)
                            console.log(xhr.responseText)
                        }
                    });
                }
                else {
                    alert("Ingresa un correo válido !")
                }
            }
        }
        else {            
            alert("Todos los campos deben ser llenados.")            
        }        
    });


});

function LimpiarCampos() {
    $('#nombre').val('');
    $('#correo').val('');
    $('#telefono').val('');
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function ValidateEmail(inputText) {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (inputText.match(mailformat)) {
        //alert("Valid email address!");
        //document.form1.text1.focus();
        return true;
    }
    else {
        
        //document.form1.text1.focus();
        return false;
    }
    
}

