import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Conversatorio3Component } from './conversatorio3.component';

describe('Conversatorio3Component', () => {
  let component: Conversatorio3Component;
  let fixture: ComponentFixture<Conversatorio3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Conversatorio3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Conversatorio3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
