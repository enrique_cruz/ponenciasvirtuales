import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
import { worker } from 'cluster';
import { createR3ProviderExpression } from '@angular/compiler';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {

  constructor(private service:SharedService) { }

  RegistroList: any = [];  

  DATE_RANGE : any = {
    FechaInicial:"",    
    FechaFinal:""
  };
  

  ngOnInit(): void {
    this.refresList();
  }

  refresList(){
    this.service.getRegistroList().subscribe(
      data=>{
        this.RegistroList = data;
      },
      error => {
        alert("Hubo un error al actualizar la lista !")
      }
    );    
  }

  getListByFecha()
  {
    
    if(this.DATE_RANGE.FechaInicial && this.DATE_RANGE.FechaFinal)
    {
      if(new Date(this.DATE_RANGE.FechaInicial).getTime() > new Date(this.DATE_RANGE.FechaFinal).getTime())
      {        
        alert("La fecha de inicio no debe ser mayor a la fecha de fin.")
      }
      else
      {
        console.log(this.DATE_RANGE.FechaInicial);
        console.log(this.DATE_RANGE.FechaFinal);
        console.log("Convertidos a fecha")
      
        this.service.getRegistroListByFecha(this.DATE_RANGE).subscribe(
          data => {
            this.RegistroList = data
            console.log("GetListByFecha: " + this.RegistroList);
          },
          error => {
            console.log("Error: " + error)         
          }
          
        );
      }
    }
    else
    {
      alert("Debes indicar una fecha de inicio y una fecha de fin.")
    }
  }
 


  descargarExcel(){

    // this.getListByFecha();
    // console.log( "ContactList: " + this.ContactoList)
    if(this.DATE_RANGE.FechaInicial && this.DATE_RANGE.FechaFinal)
    {
      if(new Date(this.DATE_RANGE.FechaInicial).getTime() > new Date(this.DATE_RANGE.FechaFinal).getTime())
      {        
        alert("La fecha de inicio no debe ser mayor a la fecha de fin.")
      }
      else
      {
        this.service.getRegistroListByFecha(this.DATE_RANGE).subscribe(
          data => {
            this.RegistroList = data;
            console.log("GetListByFecha: " + JSON.stringify(data));
      
            this.crearXSLS(this.RegistroList);

          },
          error => {
            console.log("Error: " + error)         
          }
          
        );
      }
    }
    else
    {
      alert("Debes indicar una fecha de inicio y una fecha de fin.")
    }
       
  }


  crearXSLS(data: any)
  {
    //const title= "Lista de contacto - " + (new Date(Date.now()).toLocaleString);
    const header= ["Id","Nombre","Apellido Paterno","Apellido Materno","Carrera", "Institución", "Correo", "Nivel Educativo", "Grado", "Fecha de registro"]
    //creamos el workbook
    let workbook= new Workbook();
    //Agregamos el worksheet al workbook
    let worksheet = workbook.addWorksheet("Datos de contaco");

    //Nueva fila con el titulo
    //let titleRow = worksheet.addRow([title]);
    worksheet.addRow([]);
    
    //Header
    let headerRow = worksheet.addRow(header);
    
    //Agregamos los datos
    //worksheet.addRows(this.ContactoList);    

    for (let x1 of data)
    {          
      let x2 = Object.keys(x1);      
      let temp=[];
      for(let y of x2)
      {        
        temp.push(x1[y]);
      }      
      worksheet.addRow(temp);
    }

    //download file
    let fname = "Datos de registro_" + new Date(Date.now()).toLocaleDateString;

    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'}); 
      fs.saveAs(blob, fname + '-' + new Date().valueOf() + '.xlsx');     
    });
  }  

}
