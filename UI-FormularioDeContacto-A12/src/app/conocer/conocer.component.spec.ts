import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CONOCERComponent } from './conocer.component';

describe('CONOCERComponent', () => {
  let component: CONOCERComponent;
  let fixture: ComponentFixture<CONOCERComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CONOCERComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CONOCERComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
