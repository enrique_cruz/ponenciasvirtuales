import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CONALEPComponent } from './conalep.component';

describe('CONALEPComponent', () => {
  let component: CONALEPComponent;
  let fixture: ComponentFixture<CONALEPComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CONALEPComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CONALEPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
