import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { SharedService } from './shared.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReportesComponent } from './reportes/reportes.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { LandingComponent } from './landing/landing.component';
import { AlianzaComponent } from './alianza/alianza.component';
import { PonenciasComponent } from './ponencias/ponencias.component';
import { CamexaComponent } from './camexa/camexa.component';
import { STEMComponent } from './stem/stem.component';
import { SEMSComponent } from './sems/sems.component';
import { CONOCERComponent } from './conocer/conocer.component';
import { COPARMEXComponent } from './coparmex/coparmex.component';
import { CONALEPComponent } from './conalep/conalep.component';
import { CceComponent } from './cce/cce.component';
import { CECyTEsComponent } from './cecy-tes/cecy-tes.component';
import { Conversatorio1Component } from './conversatorio1/conversatorio1.component';
import { Conversatorio2Component } from './conversatorio2/conversatorio2.component';
import { Conversatorio3Component } from './conversatorio3/conversatorio3.component';
import { ConcursoComponent } from './concurso/concurso.component';
import { ParticipantesComponent } from './participantes/participantes.component';



@NgModule({
  declarations: [
    AppComponent,
    ReportesComponent,
    LoginComponent,
    HomeComponent,
    LandingComponent,
    AlianzaComponent,
    PonenciasComponent,
    CamexaComponent,
    STEMComponent,
    SEMSComponent,
    CONOCERComponent,
    COPARMEXComponent,
    CONALEPComponent,
    CceComponent,
    CECyTEsComponent,
    Conversatorio1Component,
    Conversatorio2Component,
    Conversatorio3Component,
    ConcursoComponent,
    ParticipantesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
