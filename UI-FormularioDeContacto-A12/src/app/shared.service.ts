import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
 // readonly APIUrl="http://23.102.112.5/api/api";
 readonly APIUrl ="https://www.chicasduales.com.mx/api/api"; 
 // readonly APIUrl = "https://189.203.240.97/contacto/api"
  //readonly APIUrl = "https://monitor.grupopissa.com.mx/contacto/api"
  
  constructor(private http:HttpClient) { }


  getContactoList():Observable<any []>{
    return this.http.get<any>(this.APIUrl+'/Contacto');
  }

  getContactoListByFecha(val: any):Observable<any []>{
    return this.http.post<any>(this.APIUrl + '/Contacto/GetByDate/' , val );
  }

  addContacto(val:any){
    return this.http.post(this.APIUrl + '/Contacto', val);
  }

  updateContacto(val: any){
    return this.http.put(this.APIUrl+'/Contacto', val);
  }

  deleteContacto(val:any){
    return this.http.delete(this.APIUrl+'/Contacto/' + val);
  }

  getUsuarioList():Observable<any []>{
    return this.http.get<any>(this.APIUrl+'/Usuario');
  }

  registro(val:any){
    return this.http.post(this.APIUrl + '/Registro/Nuevo', val);
  }

  getRegistroList():Observable<any []>{
    return this.http.get<any>(this.APIUrl+'/Registro/All');
  }

  getRegistroListByFecha(val: any):Observable<any []>{
    return this.http.post<any>(this.APIUrl + '/Registro/Date' , val );
  }


}
