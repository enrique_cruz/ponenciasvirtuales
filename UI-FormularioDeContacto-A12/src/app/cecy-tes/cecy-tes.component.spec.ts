import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CECyTEsComponent } from './cecy-tes.component';

describe('CECyTEsComponent', () => {
  let component: CECyTEsComponent;
  let fixture: ComponentFixture<CECyTEsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CECyTEsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CECyTEsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
