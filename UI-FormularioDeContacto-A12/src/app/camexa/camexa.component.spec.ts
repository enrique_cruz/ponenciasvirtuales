import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CamexaComponent } from './camexa.component';

describe('CamexaComponent', () => {
  let component: CamexaComponent;
  let fixture: ComponentFixture<CamexaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CamexaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CamexaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
