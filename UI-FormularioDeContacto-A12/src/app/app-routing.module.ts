import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportesComponent } from './reportes/reportes.component'
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { LandingComponent } from './landing/landing.component';
import { AlianzaComponent } from './alianza/alianza.component';
import { PonenciasComponent } from './ponencias/ponencias.component';
import { CamexaComponent } from './camexa/camexa.component';
import { SEMSComponent } from './sems/sems.component';
import { STEMComponent } from './stem/stem.component';
import { CONOCERComponent } from './conocer/conocer.component';
import { COPARMEXComponent } from './coparmex/coparmex.component';
import { CONALEPComponent } from './conalep/conalep.component';
import { Conversatorio1Component } from './conversatorio1/conversatorio1.component';
import { Conversatorio2Component } from './conversatorio2/conversatorio2.component';
import { Conversatorio3Component } from './conversatorio3/conversatorio3.component';
import { CECyTEsComponent } from './cecy-tes/cecy-tes.component';
import { CceComponent } from './cce/cce.component';
import { ConcursoComponent } from './concurso/concurso.component';
import { ParticipantesComponent } from './participantes/participantes.component';

const routes: Routes = [
  {path:'Reportes', component: ReportesComponent},
  {path:'Login', component: LoginComponent},
  {path:'Home', component: HomeComponent},
  {path:'Main', component: LandingComponent},
  {path:'alianza', component: AlianzaComponent},
  {path:'CAMEXA', component: CamexaComponent},
  {path:'COPARMEX', component: COPARMEXComponent},
  {path:'CONOCER', component: CONOCERComponent},
  {path:'CONALEP', component: CONALEPComponent},
  {path:'SEMS', component: SEMSComponent},
  {path:'STEM', component: STEMComponent},
  {path:'C1', component: Conversatorio1Component},
  {path:'C2', component: Conversatorio2Component},
  {path:'C3', component: Conversatorio3Component},
  {path:'CECYTes', component: CECyTEsComponent},
  {path:'Concurso', component: ConcursoComponent},
  {path:'CCE', component: CceComponent},
  {path:'cine', component: PonenciasComponent},
  {path:'Participantes', component: ParticipantesComponent},
  {path: '', redirectTo: 'Main', pathMatch: 'full'},
  {path: '**', redirectTo: 'Main', pathMatch: 'full' },

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
