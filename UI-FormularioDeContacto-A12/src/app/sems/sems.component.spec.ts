import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SEMSComponent } from './sems.component';

describe('SEMSComponent', () => {
  let component: SEMSComponent;
  let fixture: ComponentFixture<SEMSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SEMSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SEMSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
