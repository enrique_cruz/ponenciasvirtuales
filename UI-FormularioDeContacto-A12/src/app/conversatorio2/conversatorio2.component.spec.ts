import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Conversatorio2Component } from './conversatorio2.component';

describe('Conversatorio2Component', () => {
  let component: Conversatorio2Component;
  let fixture: ComponentFixture<Conversatorio2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Conversatorio2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Conversatorio2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
