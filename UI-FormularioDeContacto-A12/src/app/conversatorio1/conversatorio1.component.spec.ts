import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Conversatorio1Component } from './conversatorio1.component';

describe('Conversatorio1Component', () => {
  let component: Conversatorio1Component;
  let fixture: ComponentFixture<Conversatorio1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Conversatorio1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Conversatorio1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
