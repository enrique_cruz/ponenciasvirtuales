import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cce',
  templateUrl: './cce.component.html',
  styleUrls: ['./cce.component.css']
})
export class CceComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  
  scroll(el: HTMLElement) {
    el.scrollIntoView();
}

}
