import { ComponentFixture, TestBed } from '@angular/core/testing';

import { COPARMEXComponent } from './coparmex.component';

describe('COPARMEXComponent', () => {
  let component: COPARMEXComponent;
  let fixture: ComponentFixture<COPARMEXComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ COPARMEXComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(COPARMEXComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
