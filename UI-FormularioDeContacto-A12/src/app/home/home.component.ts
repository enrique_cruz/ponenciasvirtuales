import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private service:SharedService) {   }

  CONTACT_DATA : any = {
    nombre:"",
    apellidoPaterno:"",
    apellidoMaterno:"",
    carrera:"",
    nombreInstitucion:"",
    correo:"",
    nivelEducativo:"",
    grado:""
  };
  

  ngOnInit(): void {
  }

  validarDatosContaco(){
    
    //VALIDAMOS QUE TODOS LOS CAMPOS ESTEN LLENOS
    if(this.CONTACT_DATA.nombre && this.CONTACT_DATA.apellidoPaterno && this.CONTACT_DATA.apellidoMaterno && this.CONTACT_DATA.carrera && 
       this.CONTACT_DATA.nombreInstitucion && this.CONTACT_DATA.correo && this.CONTACT_DATA.nivelEducativo)
    {
      console.log("Datos válidos !");   
      console.log(this.CONTACT_DATA); 

      //VALIDAMOS QUE EL CORREO VENGA EN EL FORMATO CORRECTO
      if(this.ValidaEmail(this.CONTACT_DATA.correo))
      {
        this.service.registro(this.CONTACT_DATA).subscribe(
          res=>{
            alert("Datos enviados con éxito !")
            this.CONTACT_DATA.nombre = '';
            this.CONTACT_DATA.apellidoPaterno = '';
            this.CONTACT_DATA.apellidoMaterno = '';
            this.CONTACT_DATA.carrera = '';
            this.CONTACT_DATA.nombreInstitucion = '';
            this.CONTACT_DATA.correo = '';
            this.CONTACT_DATA.nivelEducativo = '';
            this.CONTACT_DATA.grado = '';
          
          },
          error=>{
            alert("Hubo un error al guardar los datos !")
          }
        );
      }
      else
      {
        alert("Ingresa un correo válido !")
      }
      
    }
    else
    {
      alert("Todos los datos son requeridos");
      console.log(this.CONTACT_DATA);
    }
    
  }

  ValidaEmail(correo:string)
  {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (correo.match(mailformat)) {     
      return true;
    }
    else {          
      return false;
    }    
  }

}
