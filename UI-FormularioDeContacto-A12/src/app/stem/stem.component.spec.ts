import { ComponentFixture, TestBed } from '@angular/core/testing';

import { STEMComponent } from './stem.component';

describe('STEMComponent', () => {
  let component: STEMComponent;
  let fixture: ComponentFixture<STEMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ STEMComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(STEMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
